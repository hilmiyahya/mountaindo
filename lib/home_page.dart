import 'package:flutter/material.dart';
import 'package:mountaindo/second_page.dart';

class HomePage extends StatefulWidget {
  String value;
  HomePage({this.value});
  @override
  _HomePageState createState() => _HomePageState(value);
}

class _HomePageState extends State<HomePage> {
  String value;
  _HomePageState(this.value);

  List<Container> daftargunung = new List();
  var gunung=[
    {"nama":"Papandayan", "gambar":"papandayan.jpg"},
    {"nama":"Prau", "gambar":"prau.jpg"},
    {"nama":"Rinjani", "gambar":"rinjani.jpg"},
    {"nama":"Semeru", "gambar":"semeru.jpg"},
  ];
  _buatlist()async{
    for (var i = 0; i < gunung.length; i++){
      final pegunungan = gunung[i];
      final String gambar = pegunungan["gambar"];
      daftargunung.add(
          new Container(
            padding: new EdgeInsets.all(15.0),
            child: new Card(
                child:
                  new Column(
                    children: <Widget>[
                      new Hero(
                        tag: pegunungan["nama"],
                          child: new Material(
                            child: new InkWell(
                              onTap: ()=> Navigator.of(context).push(
                                  new MaterialPageRoute(builder: (BuildContext context) =>
                                  new SecondPage( nama: pegunungan["nama"], gambar: gambar,),
                                  )),
                              child: new Image.asset("assets/img/$gambar", fit: BoxFit.cover,),
                            ),
                          )
                      ),

                      new Padding(padding: new EdgeInsets.all(8.0)),
                      new Text(pegunungan["nama"], style: new TextStyle(fontSize: 15.0),)
                    ],
                  )
            )
          )
      );
    }
  }

  @override
  void initState(){
    _buatlist();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body:SafeArea(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: 250,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      image: DecorationImage(
                          image: AssetImage('assets/img/home.jpg'),
                          fit: BoxFit.cover
                      )
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: LinearGradient(
                            begin: Alignment.bottomRight,
                            colors: [
                              Colors.black.withOpacity(.4),
                              Colors.black.withOpacity(.2),
                            ]
                        )
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[

                        Text("Enjoy Your Life", style: TextStyle(color: Colors.white, fontSize: 35, fontWeight: FontWeight.bold),),
                        Text("&",style: TextStyle(color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold),),
                        Text("Welcome "+ value,style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.normal),),
                        SizedBox(height: 100,),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 40,),
                Text("Cari Tahu Aja Dulu!", style: TextStyle(color: Colors.black, fontSize: 25, fontWeight: FontWeight.bold),),
                SizedBox(height: 10,),
                Expanded(
                    child: new GridView.count(
                      crossAxisCount: 2,
                      children: daftargunung,
                    ),
                )
              ],
            ),
          ),
        ),
    );
  }
}



